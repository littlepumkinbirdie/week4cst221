//
//  main.c
//  Activity4
//
//  Created by Josh Beck on 10/21/20.
//
//  This program accepts input from the user and manually transforms the decimal
//  value into a binary and tranforms the decimal into a hexadecimal through the
//  provided string translations.  It does this through transformation
//  between the bases and will output the transformations.  Additionally, we
//  experiment with bit masks and operations in order to manipulate the answer
//  entered in at the beginning of the program.


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdint.h>

/**
 This method converts a unsigned 32 bit integer to a binary.  It does this by creating a for-loop to iterate through a full 32-bits and checks the number using a mask.  If the number after being masked is 1, it displays the value; same if the mask reveals a 0 then a 0 is printed.  However, it will not start displaying 0s until at least 1 '1' is found.  This will help to remove leading 0s
 */
void displayBinary(unsigned num){
   
    //Holds whether to start saving 0s to the string to trim unnecessary leading 0s
    int startSaving = 0;
    
    
    //Taken from in portion - https://www.geeksforgeeks.org/binary-representation-of-a-given-number/
    unsigned i;
    for (i = 1 << 31; i > 0; i = i / 2){
        
        //As soon as a 1 is seen, go ahead and start appending digits to the string to trim unnecessary leading 0s
        if ((num & i)){
            startSaving = 1;
            printf("%d", 1);
        } else {
            if (startSaving){
                //Start displaying the 0s
                printf("%d", 0);
            }
        }
        
    }

}

int handleProgram(){
    printf("Please enter a decimal between 0 and 4095...");

    uint32_t answer;

    if (scanf("%d", &answer) != 0){
        //The number was successfully saved
        
        if (answer >= 0 && answer <= 4095){
            //Continue as the number is safe and within range
            printf("The entered number (%d) to binary is 0x", answer);
            //Print the binary
            displayBinary(answer);
            printf("\n");
            
            //Display the number in its corrosponding HEX value
            printf("The hex value you typed in is %x\n", answer);
           
            /*Transform the input by shifting the number 16 bits to the left, then mask out (AND) the bottom 16 bits, and finally add (OR) the hex number 0x07FF to produce the final resultant number. Display the final result in binary, hexadecimal, and decimal to the console.*/
            printf("Shifting answer 16 bits to the left...\n");
            //Shift the number 16 to the left (add 16 zeros on the right of the number)
            answer <<= 16;
            printf("Current number is %d\n", answer);
            
            printf("Masking out (removing) the bottom 16 digits...\n");
            //Removing the bottom 16 bits (HEX representation of 16 ones followed by 16 digits)
            //Remove the bottom 16 bits... ~0xFFFF is equal to the compliment of 0xFFFF so it is equatable in a 32-bit operating system to 0xFFFF0000
            answer &= ~0xFFFF;
            printf("Current numer is %d\n", answer);
            
            printf("Adding the hex number 0x07FF...\n");
            //Add the hex number
            answer |= 0x07FF;
            
            printf("Final answer after all manipulations is decimal: %d, binary: 0b", answer);
            displayBinary(answer);
            printf(", HEX: 0x%x\n", answer);
            
        } else {
            //The number was outside the range so re-run the program
            printf("The number entered was not between 0 and 4095... try again...");
            handleProgram();
        }
        
    } else {
        //The number entered was not a number
        printf("The number entered was not a number... try again...");
        handleProgram();
    }
    return 0;
}

int main(){
     handleProgram();
}

